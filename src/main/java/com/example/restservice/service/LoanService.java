package com.example.restservice.service;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.example.restservice.metrics.ILoanMetricCalculator;
import com.example.restservice.metrics.LoanMetricFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.example.restservice.model.Loan;
import com.example.restservice.model.LoanMetric;
import com.example.restservice.util.LoanGeneratonUtil;

@Service
public class LoanService {

	@Autowired
	private LoanMetricFactory loanMetricFactory;


	public Loan getLoan(Long id) {
		return LoanGeneratonUtil.createLoan(id);
	}

	public LoanMetric calculateLoanMetric(Loan loan) {
		ILoanMetricCalculator loanMetricCalculator = loanMetricFactory.getInstance(loan);
		return loanMetricCalculator.getLoanMetric(loan);
	}

	public LoanMetric calculateLoanMetric(Long loanId) {
		Loan loan = getLoan(loanId);
		return calculateLoanMetric(loan);
	}

	public Loan getMaxMonthlyPaymentLoan() {

		List<Loan> allLoans = LoanGeneratonUtil.getRandomLoans(20L);

		Map<Double,Loan> object = allLoans.stream().collect(Collectors.toMap(
				x -> calculateLoanMetric(x).getMonthlyPayment(),
				x ->x ));
//		object.entrySet().forEach(doubleLoanEntry -> {
//			System.out.println("LoanId " + doubleLoanEntry.getValue().getLoanId() + " payment:" + doubleLoanEntry.getKey());
//		});
		return object.entrySet().stream().max((x,y) -> x.getKey() > y.getKey()?1:-1).get().getValue();
		//System.out.println("el id del maximo" + object.entrySet().stream().max((x,y) -> x.getKey() > y.getKey()?1:-1).get().getValue().getLoanId() );
//
//		Loan loan = allLoans.stream().collect(Collectors.toMap(
//				x -> x,
//				x -> calculateLoanMetric(x).getMonthlyPayment()))
//				.entrySet().stream().max((entry1, entry2) ->  entry1.getValue() > entry2.getValue() ? 1 : -1).get().getKey();

		//return loan;

	}
}
