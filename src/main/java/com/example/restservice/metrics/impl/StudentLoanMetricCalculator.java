package com.example.restservice.metrics.impl;

import com.example.restservice.model.LoanType;
import org.springframework.stereotype.Component;

import com.example.restservice.metrics.ILoanMetricCalculator;
import com.example.restservice.model.Loan;
import com.example.restservice.model.LoanMetric;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;

@Component("student")
public class StudentLoanMetricCalculator implements ILoanMetricCalculator {


	@Override
	public LoanMetric getLoanMetric(Loan loan) {
		//Using big decimal for better handling of the decimals and computations.

		//1 get monthly rate then divide by 100. Because of rounding it's better to split into var for better understanding
		BigDecimal montlyRate = new BigDecimal(loan.getAnnualInterest()).divide(new BigDecimal(12),2,RoundingMode.DOWN);
		BigDecimal monthyInterestRate = montlyRate.divide(new BigDecimal(100),10,RoundingMode.UP);

		//split divide operation into two variable for better code understanding
		BigDecimal numerator =new BigDecimal(0.8).multiply(new BigDecimal(loan.getRequestedAmount()).multiply(monthyInterestRate));
		BigDecimal temp = BigDecimal.ONE.add(monthyInterestRate);

		BigDecimal pow = temp.pow(-loan.getTermMonths(),new MathContext(10));
		BigDecimal denominator = BigDecimal.ONE.subtract(pow);

		BigDecimal result = numerator.divide(denominator,1, RoundingMode.DOWN);
		return new LoanMetric(monthyInterestRate.doubleValue(), result.doubleValue());
	}

	@Override
	public String getCalculatorType() {
		return LoanType.STUDENT.getName();
	}

	@Override
	public boolean isSupported(Loan loan) {
		return ILoanMetricCalculator.super.isSupported(loan) &&
		 loan.getBorrower().getAge() > 18 && loan.getBorrower().getAge() < 30;
	}

}
