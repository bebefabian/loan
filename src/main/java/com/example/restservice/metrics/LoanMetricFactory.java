package com.example.restservice.metrics;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.example.restservice.model.Loan;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
public class LoanMetricFactory {


   private final Map<String, ILoanMetricCalculator> calculatorsByType;

   @Autowired
   public LoanMetricFactory(List<ILoanMetricCalculator> calculators) {
      calculatorsByType = calculators.stream()
              .collect(Collectors.toMap(ILoanMetricCalculator::getCalculatorType, Function.identity()));
   }

   public ILoanMetricCalculator getInstance(Loan loan) {
      return calculatorsByType.get(loan.getType());
   }

}
