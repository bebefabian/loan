package com.example.restservice.controller;

import com.example.restservice.service.LoanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.example.restservice.model.Loan;
import com.example.restservice.model.LoanMetric;

@RestController("/loans")
public class LoanController {

	@Autowired
	private LoanService loanService;

	@GetMapping("/{loanId}")
	public Loan getLoan(@PathVariable Long loanId) {
		return loanService.getLoan(loanId);
	}

	@PostMapping("/{loanId}/metric")
	public LoanMetric calculateLoanMetric(@PathVariable Long loanId) {
		return loanService.calculateLoanMetric(loanId);
	}

	@PostMapping("/metric")
	public LoanMetric calculateLoanMetric(@RequestBody Loan loan) {
		return loanService.calculateLoanMetric(loan);
	}

	@GetMapping("/max")
	public Loan getMaxMonthlyPaymentLoan() {
		return loanService.getMaxMonthlyPaymentLoan();
	}

}
