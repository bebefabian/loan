package com.example.restservice.model;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;

public enum LoanType {

    STUDENT("student"),
    CONSUMER("consumer");
    private static final Map<String, LoanType> map = new HashMap<>();


    private String name;

    static{
        for(LoanType ddt : EnumSet.allOf(LoanType.class)){
            map.put(ddt.name, ddt);
        }
    }

    public static LoanType get(String name){
        return map.get(name);
    }

     LoanType(String name){
        this.name = name;
    }

    public String getName(){
         return name;
    }

}
