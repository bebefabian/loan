package com.example.restservice;

import com.example.restservice.metrics.ILoanMetricCalculator;
import com.example.restservice.model.Borrower;
import com.example.restservice.model.Loan;
import com.example.restservice.model.LoanMetric;
import com.example.restservice.model.LoanType;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest
@RunWith(SpringRunner.class)
public class TestStudentLoanCalculator {

    @Autowired
    @Qualifier("student")
    public ILoanMetricCalculator iLoanMetricCalculator;

    @Test
    public void monthlyInterestRateShouldBeZeroFive() {
        Loan loan = new Loan();
        loan.setTermMonths(24);
        loan.setAnnualInterest(6.00);
        loan.setType("student");

        loan.setRequestedAmount(10000.00);
        LoanMetric loanMetric = iLoanMetricCalculator.getLoanMetric(loan);
        Assert.assertEquals(new Double(0.005), loanMetric.getMonthlyInterestRate());

    }

    @Test
    public void given40YearStudentIsSupportedShouldfailed() {
        Loan loan = new Loan();
        loan.setType(LoanType.STUDENT.getName());
        Borrower borrower = new Borrower();
        borrower.setAge(40);
        loan.setBorrower(borrower);
        Assert.assertFalse(iLoanMetricCalculator.isSupported(loan));
    }

    @Test
    public void given19YearStudentIsSupportedShouldPass() {
        Loan loan = new Loan();
        loan.setType(LoanType.STUDENT.getName());
        Borrower borrower = new Borrower();
        borrower.setAge(19);
        loan.setBorrower(borrower);
        Assert.assertTrue(iLoanMetricCalculator.isSupported(loan));
    }


    @Test
    public void monthlyInteresPaymentShouldBeFourFourThree() {
        Loan loan = new Loan();
        loan.setTermMonths(24);
        loan.setAnnualInterest(6.00);
        loan.setType("student");

        loan.setRequestedAmount(10000.00);
        LoanMetric loanMetric = iLoanMetricCalculator.getLoanMetric(loan);
        Assert.assertEquals(new Double(354.5), loanMetric.getMonthlyPayment());

    }


}
