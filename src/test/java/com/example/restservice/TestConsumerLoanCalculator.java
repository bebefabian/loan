package com.example.restservice;

import com.example.restservice.metrics.ILoanMetricCalculator;
import com.example.restservice.model.Loan;
import com.example.restservice.model.LoanMetric;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest
@RunWith(SpringRunner.class)
public class TestConsumerLoanCalculator {

    @Autowired
    @Qualifier("consumer")
    public ILoanMetricCalculator iLoanMetricCalculator;

    @Test
    public void monthlyInterestRateShouldBeZeroFive(){
        Loan loan = new Loan();
        loan.setTermMonths(24);
        loan.setAnnualInterest(6.00);
        loan.setType("consumer");

        loan.setRequestedAmount(10000.00);
        LoanMetric loanMetric = iLoanMetricCalculator.getLoanMetric(loan);
        Assert.assertEquals(new Double(0.005),loanMetric.getMonthlyInterestRate());

    }


    @Test
    public void monthlyInteresPaymentShouldBeFourFourThree(){
        Loan loan = new Loan();
        loan.setTermMonths(24);
        loan.setAnnualInterest(6.00);
        loan.setType("consumer");

        loan.setRequestedAmount(10000.00);
        LoanMetric loanMetric = iLoanMetricCalculator.getLoanMetric(loan);
        Assert.assertEquals(new Double(443.20),loanMetric.getMonthlyPayment());

    }


}
