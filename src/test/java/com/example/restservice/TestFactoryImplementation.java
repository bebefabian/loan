package com.example.restservice;

import com.example.restservice.metrics.ILoanMetricCalculator;
import com.example.restservice.metrics.LoanMetricFactory;
import com.example.restservice.metrics.impl.ConsumerLoanMetricCalculator;
import com.example.restservice.metrics.impl.StudentLoanMetricCalculator;
import com.example.restservice.model.Loan;
import com.example.restservice.model.LoanType;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest
@RunWith(SpringRunner.class)
public class TestFactoryImplementation {

    @Autowired
    public LoanMetricFactory factory;


    @Test
    public void shouldCreateStudnetCalculatorGivenStudentType(){
        Loan loan = new Loan();
        loan.setType(LoanType.STUDENT.getName());
        ILoanMetricCalculator calculator = factory.getInstance(loan);
        boolean isInstance = calculator instanceof StudentLoanMetricCalculator;
        Assert.assertTrue(isInstance);
    }

    @Test
    public void shouldCreateConsumerCalculatorGivenConsumerType(){
        Loan loan = new Loan();
        loan.setType(LoanType.CONSUMER.getName());
        ILoanMetricCalculator calculator = factory.getInstance(loan);
        boolean isInstance = calculator instanceof ConsumerLoanMetricCalculator;
        Assert.assertTrue(isInstance);
    }

    @Test
    public void shouldFailConsumerCalculatorGivenStudentType(){
        Loan loan = new Loan();
        loan.setType(LoanType.STUDENT.getName());
        ILoanMetricCalculator calculator = factory.getInstance(loan);
        boolean isInstance = calculator instanceof ConsumerLoanMetricCalculator;
        Assert.assertFalse(isInstance);
    }

    @Test
    public void shouldFailStudentCalculatorGivenConsumerType(){
        Loan loan = new Loan();
        loan.setType(LoanType.CONSUMER.getName());
        ILoanMetricCalculator calculator = factory.getInstance(loan);
        boolean isInstance = calculator instanceof StudentLoanMetricCalculator;
        Assert.assertFalse(isInstance);
    }

}
